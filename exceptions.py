class InfiniteMapException(Exception):
    description = "The map does not a have a treasure it is infinite"


class NotSymmetricalMapException(Exception):
    description = "The map cannot be processed. It's not symmetrical"


class MapTooBig(Exception):
    description = "The Map edge cannot be more the 9 tiles"


class TileTooLargeException(Exception):
    description = "numbers cannot be more then 99"
