from functional import preprocess_map, tile_check
from object import TreasureHunter

your_map = [55, 14, 25, 52, 21,
            44, 31, 11, 53, 43,
            24, 13, 45, 12, 34,
            42, 22, 43, 32, 41,
            51, 23, 33, 54, 15]
# func
PPed_map = preprocess_map(your_map)
treasure, route = tile_check(PPed_map)
print(treasure)
print(route)

# object
th = TreasureHunter.preprocess_map(your_map)
th.hunt()
print(th.answer)