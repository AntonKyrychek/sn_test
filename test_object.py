from object import TreasureHunter
import pytest
from exceptions import InfiniteMapException, NotSymmetricalMapException, MapTooBig, TileTooLargeException
from test_variables import *


class Test4TH:
    good_th: TreasureHunter = None
    answer = 43
    route = [11, 55, 15, 21, 44, 32, 13, 25, 43]
    th = TreasureHunter

    def test_preprocess_map_5x5(self):
        test_th = self.th.preprocess_map(good_map)
        assert PPed_map == test_th._map
        self.good_th = test_th

    def test_preprocess_map_Exseption(self):
        with pytest.raises(NotSymmetricalMapException):
            assert self.th.preprocess_map(bad_map)

    def test_preprocess_map_Too_big(self):
        with pytest.raises(MapTooBig):
            assert self.th.preprocess_map(too_big_map)

    def test_preprocess_map_7x7(self):
        test_th = self.th.preprocess_map(map_7x7)
        assert PPed_map_7x7 == test_th._map

    def test_preprocess_map(self):
        assert self.answer, self.route == self.good_th.hunt()

    def test_preprocess_map_Exception_infinite(self):
        test_th = self.th(PPed_infini_map)
        with pytest.raises(InfiniteMapException):
            assert test_th.hunt()

    def test_preprocess_map_TileTooLargeException(self):
        test_th = self.th(tile_too_large_PPed_map)
        with pytest.raises(TileTooLargeException):
            assert test_th.hunt()


    def test_preprocess_map_TileTooLargeException1(self):
        test_th = self.th.preprocess_map(tile_too_large_map)
        with pytest.raises(TileTooLargeException):
            assert test_th.hunt()
