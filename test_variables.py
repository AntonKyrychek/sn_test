good_map = [55, 14, 25, 52, 21,
            44, 31, 11, 53, 43,
            24, 13, 45, 12, 34,
            42, 22, 43, 32, 41,
            51, 23, 33, 54, 15, ]
bad_map = [55, 14, 25, 52, 21,
           44, 31, 11, 53, 43,
           24, 13, 45, 12, 34,
           42, 22, 43, 32, 41,
           51, 23, 33, 54]
PPed_map = [[55, 14, 25, 52, 21],
            [44, 31, 11, 53, 43],
            [24, 13, 45, 12, 34],
            [42, 22, 43, 32, 41],
            [51, 23, 33, 54, 15], ]

map_7x7 = [55, 14, 25, 52, 21, 11, 21,
           44, 31, 11, 53, 43, 11, 21,
           24, 13, 45, 12, 34, 11, 21,
           42, 22, 43, 32, 41, 11, 21,
           51, 23, 33, 54, 15, 11, 21,
           51, 23, 33, 54, 15, 11, 21,
           51, 23, 33, 54, 15, 11, 21, ]
PPed_map_7x7 = [[55, 14, 25, 52, 21, 11, 21],
                [44, 31, 11, 53, 43, 11, 21],
                [24, 13, 45, 12, 34, 11, 21],
                [42, 22, 43, 32, 41, 11, 21],
                [51, 23, 33, 54, 15, 11, 21],
                [51, 23, 33, 54, 15, 11, 21],
                [51, 23, 33, 54, 15, 11, 21], ]
infini_map = [55, 14, 25, 52, 21,
              44, 31, 11, 53, 44,
              24, 13, 45, 12, 34,
              42, 22, 43, 32, 41,
              51, 23, 33, 54, 15, ]
too_big_map = [55, 14, 25, 52, 21, 55, 14, 25, 52, 21,
               44, 31, 11, 53, 43, 55, 14, 25, 52, 21,
               24, 13, 45, 12, 34, 55, 14, 25, 52, 21,
               42, 22, 43, 32, 41, 55, 14, 25, 52, 21,
               51, 23, 33, 54, 41, 55, 14, 25, 52, 21,
               51, 23, 33, 54, 41, 55, 14, 25, 52, 21,
               51, 23, 33, 54, 41, 55, 14, 25, 52, 21,
               51, 23, 33, 54, 41, 55, 14, 25, 52, 21,
               51, 23, 33, 54, 41, 55, 14, 25, 52, 21,
               51, 23, 33, 54, 41, 55, 14, 25, 52, 21,
               ]
PPed_infini_map = [[55, 14, 25, 52, 21],
            [44, 31, 11, 53, 44],
            [24, 13, 45, 12, 34],
            [42, 22, 43, 32, 41],
            [51, 23, 33, 54, 15], ]
tile_too_large_map = [56, 14, 25, 52, 21,
            44, 31, 11, 53, 43,
            24, 13, 45, 12, 34,
            42, 22, 43, 32, 41,
            51, 23, 33, 54, 15, ]
tile_too_large_PPed_map = [[56, 14, 25, 52, 21, ],
                            [44, 31, 11, 53, 43, ],
                            [24, 13, 45, 12, 34, ],
                            [42, 22, 43, 32, 41, ],
                            [51, 23, 33, 54, 15, ], ]
