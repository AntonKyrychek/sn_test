import math
from abc import ABC,abstractmethod
from exceptions import InfiniteMapException, NotSymmetricalMapException, MapTooBig, TileTooLargeException


class AbstractTreasureHunter(ABC):
    _current_tile: int
    _next_tile: int
    _map: [list]
    _route: list
    _treasure: int

    @classmethod
    @abstractmethod
    def preprocess_map(cls, map):
        raise NotImplementedError

    def tile_check(self):
        raise NotImplementedError

    def hunt(self):
        raise NotImplementedError

    @property
    def answer(self):
        if not self._treasure:
            return "Go hunting!"
        return {
            "treasure": self._treasure,
            "route": self._route
        }

    @property
    def treasure(self):
        return self._treasure if self._treasure else "Go hunting!"

    @property
    def route(self):
        return self._route if self._route else "Go hunting!"


class TreasureHunter(AbstractTreasureHunter):
    _current_tile: int = 11
    _next_tile: int = None
    _map: list = None
    _route: list = None
    _treasure: int = None

    def __init__(self, m):
        self._map = m
        self._route = []

    @classmethod
    def preprocess_map(cls, m):
        PPed_map = []
        edge = int(math.sqrt(len(m)))
        if len(m) != edge ** 2:
            raise NotSymmetricalMapException
        if edge > 9:
            raise MapTooBig
        for i in range(edge):
            start = 0 + i * edge
            end = edge * (i + 1)
            PPed_map.append(m[start:end])
        return cls(PPed_map)

    def tile_check(self):
        if len(self._route) > len(self._map) ** 2:
            raise InfiniteMapException
        if self._current_tile > len(self._map) * 11:
            raise TileTooLargeException
        y = self._current_tile // 10
        x = self._current_tile % 10
        self._next_tile = self._map[y - 1][x - 1]
        self._route.append(self._current_tile)

    def hunt(self):
        self.tile_check()
        while self._current_tile != self._next_tile:
            self._current_tile = self._next_tile
            self.tile_check()
        self._treasure = self._current_tile
