import math
from exceptions import InfiniteMapException, NotSymmetricalMapException, MapTooBig, TileTooLargeException


def tile_check(map, tile=11, answer: (list, None) = None):
    route = [] if not answer else answer
    if tile > len(map) * 11:
        raise TileTooLargeException
    y = tile // 10
    x = tile % 10
    next_tile = map[y - 1][x - 1]
    route.append(tile)
    if tile != next_tile:
        if len(route) > len(map) ** 2:
            raise InfiniteMapException
        treasure, route = tile_check(map, next_tile, route)
    else:
        treasure = tile

    return treasure, route


def preprocess_map(map):
    PPed_map = []

    edge = int(math.sqrt(len(map)))
    if len(map) != edge ** 2:
        raise NotSymmetricalMapException
    if edge > 9:
        raise MapTooBig
    for i in range(edge):
        start = 0 + i * edge
        end = edge * (i + 1)
        PPed_map.append(map[start:end])

    return PPed_map
