from functional import preprocess_map, tile_check
import pytest
from exceptions import InfiniteMapException, NotSymmetricalMapException, MapTooBig, TileTooLargeException
from test_variables import *


def test_preprocess_map_5x5():
    assert PPed_map == preprocess_map(good_map)


def test_preprocess_map_Exseption():

    with pytest.raises(NotSymmetricalMapException):
        assert preprocess_map(bad_map)


def test_preprocess_map_Too_big():
    with pytest.raises(MapTooBig):
        assert preprocess_map(too_big_map)


def test_preprocess_map_7x7():
    assert PPed_map_7x7 == preprocess_map(map_7x7)


def test_preprocess_map():
    answer = 43
    route = [11, 55, 15, 21, 44, 32, 13, 25, 43]
    assert answer, route == tile_check(PPed_map)


def test_preprocess_map_exception_infinite():
    with pytest.raises(InfiniteMapException):
        assert tile_check(PPed_infini_map)


def test_preprocess_map_tiletoolargeexception():
    with pytest.raises(TileTooLargeException):
        assert tile_check(tile_too_large_PPed_map)
